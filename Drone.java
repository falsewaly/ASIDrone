//enum Direction
//	{
//		NORTH, WEST, SOUTH, EAST;
//	}
public class Drone {
	int direction;
	int y;
	int x;
	
	public Drone(int direction, int y, int x) {	
		this.direction = direction;
		this.y = y;
		this.x = x;
	}	
	
	public void rotate(boolean isRight){
		if(isRight){
			direction = (direction - 1) % 4;			
		} else {
			direction = (direction + 1) % 4;
		}		
	}
	
	public boolean moveForward(){
		switch(direction){
			case 0:
				y--;
			case 1:
				x--;
			case 2:
				y++;
			case 3:
				x++;
		}
		return true;
	}
	
	public boolean moveBackward(){
		switch(direction){
			case 0:
				y++;
			case 1:
				x++;
			case 2:
				y--;
			case 3:
				x--;
		}
		return true;
	}
	
}
