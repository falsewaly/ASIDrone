import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		//TODO: buat region
		Scanner in = new Scanner(System.in);
		System.out.println("Insert the width and length of the area ex : 6 8");
		int width = in.nextInt();
		int length = in.nextInt();
		
		//TODO: buat drone
		System.out.println("Insert the number of drone ");
		int droneNum = in.nextInt();
		Drone[] drones = new Drone[droneNum];
		
		for(int i = 0; i < droneNum; i++) {
			System.out.println("insert the position (y and x) of the drone, ex : 2 3");
			int y = in.nextInt();
			int x = in.nextInt();
			System.out.println("Insert the direction of the drone \n 0 = NORTH \n 1 = WEST 2 = SOUTH 3 = EAST");
			int dir;
			int intDir = in.nextInt();
			// Default is North
			dir = intDir < 4 && intDir >= 0 ? intDir : 0; 
//			switch(intDir){
//			case 0 :
//				dir = Direction.NORTH;
//				break;
//			case 1 :
//				dir = Direction.WEST;
//				break;
//			case 2 :
//				dir = Direction.SOUTH;
//				break;
//			case 3 :
//				dir = Direction.EAST;
//				break;
//			default :
//				dir = Direction.NORTH;
//			}
			drones[i] = new Drone(dir, y, x);
		}
		
		Region region = new Region(width, length, drones);
		DroneController dc = new DroneController(region);
		
		//TODO: Jalanin drone
		boolean finish = false;
		while(!finish) {
			System.out.println("press 0 to rotate drone press 1 to move drone");
			int command = in.nextInt();
			if(command == 0){
				System.out.println("insert drone number");
				int dNum = in.nextInt();
				System.out.println("0 = rotate to right, 1 = rotate to left");
				int rotation = in.nextInt();
				if(rotation == 0){
					dc.rotate(droneNum, true);
				}
			}
		}
		
			
	}
}
