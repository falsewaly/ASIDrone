
public class DroneController {
	Region region;

	public DroneController(Region region) {		
		this.region = region;
	}
	
	public boolean rotate(int droneNum, boolean isRight){
		if(region.drones.length < droneNum){
			return false;
		}
		Drone drone =region.drones[droneNum];
		drone.rotate(isRight);
		return true;
	}
	
	public boolean forward(int droneNum){
		if(region.drones.length < droneNum){
			return false;
		}
		Drone drone =region.drones[droneNum];
		drone.moveForward();
		if(!isValidPos(drone)){
			drone.moveBackward();
		}
		return true;
	}
	
	public boolean isValidPos(Drone drone){
		return drone.x >= 0 && drone.y >= 0 && drone.x < region.length && drone.y < region.width;
	}
	
}
